<?php

namespace App\DTO;

class BookMetadata
{
    /** @var string */
    private ?string $title;

    /** @var string */
    private ?string $lang;

    /** @var string */
    private ?string $authorName;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BookMetadata
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getLang(): ?string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     * @return BookMetadata
     */
    public function setLang(string $lang): self
    {
        $this->lang = $lang;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorName(): ?string
    {
        return $this->authorName;
    }

    /**
     * @param string $authorName
     * @return BookMetadata
     */
    public function setAuthorName(string $authorName): self
    {
        $this->authorName = $authorName;
        return $this;
    }
}