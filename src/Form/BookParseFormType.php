<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookParseFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fileConstraints = new File([
            'maxSize' => '10m',
            'maxSizeMessage' => 'Размер файла слишком велик ({{ size }} {{ suffix }}). Максимальный допустимый размер - {{ limit }} {{ suffix }}.',
        ]);
        $builder
            ->add('book_file', FileType::class, [
                'help' => 'Возможные расширения файлов для загрузки: .epub или .fb2',
                'required' => true,
                'constraints' => [
                    $fileConstraints,
                    new NotBlank()
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
