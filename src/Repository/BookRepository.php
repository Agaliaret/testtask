<?php

namespace App\Repository;

use App\Entity\Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Book::class);
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getBooksUploadsGroupedByDay(): array
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = <<<SQLSTATEMENT
SELECT DATE(b.created_date) AS date, COUNT(b.id) as book_count
FROM book b
GROUP BY DATE(b.created_date);
SQLSTATEMENT;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}
