<?php

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author[]    findAll()
 * @method Author[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author::class);
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getBooksAmountByAuthor(): array
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = <<<SQLSTATEMENT
SELECT a.name AS author_name, COUNT(b.id) as book_count
FROM author a
    INNER JOIN book b ON a.id = b.author_id
GROUP BY a.id;
SQLSTATEMENT;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }

    /**
     * @return array
     * @throws DBALException
     */
    public function getAuthorsWithoutBooks(): array
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = <<<SQLSTATEMENT
SELECT a.name AS author_name
FROM author a
    LEFT JOIN book b ON a.id = b.author_id
WHERE b.id IS NULL;
SQLSTATEMENT;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(FetchMode::ASSOCIATIVE);
    }
}
