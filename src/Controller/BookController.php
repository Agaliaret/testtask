<?php

namespace App\Controller;

use App\Form\BookParseFormType;
use App\Form\BookSearchFormType;
use App\Repository\AuthorRepository;
use App\Repository\BookRepository;
use App\Service\BookService;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Exception;
use InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BookController extends AbstractController
{
    /**
     * @Route("/book/parse", name="book_parse")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function parseBook(Request $request, EntityManagerInterface $em): Response
    {
        $form = $this->createForm(BookParseFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                /** @var UploadedFile $bookFile */
                $bookFile = $form->getData()['book_file'];
                (new BookService($em))->parseBook($bookFile);
            } catch (InvalidArgumentException $e) {
                $form->get('book_file')->addError(new FormError(
                    'Загружен некорректный файл',
                    'Загружен некорректный файл',
                ));
            } catch (DomainException $e) {
                $form->get('book_file')->addError(new FormError(
                    'Загружен файл с неподдерживаемым типом',
                    'Загружен файл с неподдерживаемым типом',
                ));
            } catch (Exception $e) {
                $form->addError(new FormError(
                    'Внутренняя ошибка сервера',
                    'Внутренняя ошибка сервера',
                ));
            }
        }

        return $this->render('book/parse/parse_book.html.twig', [
            'form_for_book_parsing' => $form->createView(),
        ]);
    }

    /**
     * @Route("/book/search", name="book_search")
     * @param Request $request
     * @param BookRepository $bookRepo
     * @return Response
     */
    public function bookSearch(Request $request, BookRepository $bookRepo): Response
    {
        $form = $this->createForm(BookSearchFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $bookTitle = $form->getData()['title'];
                $searchResult = $bookRepo->findOneBy(['title' => $bookTitle]);
                return $this->render('book/search/search_book.html.twig', [
                    'form_for_book_search' => $form->createView(),
                    'with_search_result' => true,
                    'search_result' => $searchResult
                ]);
            } catch (Exception $e) {
                return new Response('Внутренняя ошибка сервера', Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }

        return $this->render('book/search/search_book.html.twig', [
            'form_for_book_search' => $form->createView(),
            'with_search_result' => false,
        ]);
    }

    /**
     * @Route("/book/statistics", name="book_statistics")
     * @param BookRepository $bookRepo
     * @param AuthorRepository $authorRepo
     * @return Response
     * @throws DBALException
     */
    public function getBooksStatistics(BookRepository $bookRepo, AuthorRepository $authorRepo): Response
    {
        $booksByAuthor = $authorRepo->getBooksAmountByAuthor();
        $authorsWithoutBooks = $authorRepo->getAuthorsWithoutBooks();
        $booksByDay = $bookRepo->getBooksUploadsGroupedByDay();

        return $this->render('book/statistics/book_statistics.html.twig', [
            'books_by_author' => $booksByAuthor,
            'authors_without_books' => $authorsWithoutBooks,
            'books_by_day' => $booksByDay,
        ]);
    }
}
