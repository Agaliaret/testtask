<?php

namespace App\Service\Factory;

use App\Service\Parser\EpubParser;
use App\Service\Parser\FB2Parser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookParserFactory
{
    private const EPUB_FILE_EXTENSION = 'epub';
    private const FB2_FILE_EXTENSION = 'fb2';

    /**
     * Связь оригинальных расширений файлов с определёнными на основании MIME-типов
     *
     * @var array
     */
    private static array $originalToGuessedExtensionRelation = [
        self::EPUB_FILE_EXTENSION => ['zip', 'epub'],
        self::FB2_FILE_EXTENSION => ['xml'],
    ];

    public static function createByBookFile(UploadedFile $file)
    {
        self::checkFileType($file);
        switch ($file->getClientOriginalExtension()) {
            case self::EPUB_FILE_EXTENSION:
                return new EpubParser($file);
                break;
            case self::FB2_FILE_EXTENSION:
                return new FB2Parser($file);
                break;
            default:
                throw new \DomainException('Неподдерживаемый тип файла!');
        }
    }

    /**
     * Проверяем, что оригинальное расширение файла совпадает с определённым на основании MIME-типа этого файла
     *
     * @param UploadedFile $file
     * @return bool
     * @throws \DomainException
     */
    private static function checkFileType(UploadedFile $file): bool
    {
        if (
            array_key_exists($file->getClientOriginalExtension(), self::$originalToGuessedExtensionRelation) &&
            in_array(
                $file->guessExtension(),
                self::$originalToGuessedExtensionRelation[$file->getClientOriginalExtension()],
                true
            )
        ) {
            return true;
        }
        throw new \DomainException('Неподдерживаемый тип файла!');
    }
}