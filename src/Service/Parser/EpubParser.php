<?php

namespace App\Service\Parser;

use App\DTO\BookMetadata;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class EpubParser extends AbstractBookParser
{
    /**
     * @return BookMetadata
     */
    public function parse(): BookMetadata
    {
        $zip = new \ZipArchive();
        if ($zip->open($this->file->getRealPath()) !== true) {
            throw new \InvalidArgumentException('Не удалось открыть EPUB-файл');
        }
        $randomID = uniqid((string)getmypid(), false);
        $tmpPath = '/tmp/'.$randomID.'/';
        if ($zip->extractTo($tmpPath) !== true) {
            throw new \InvalidArgumentException('Не удалось распаковать EPUB-файл');
        }

        $containerXmlPath = $tmpPath . 'META-INF/container.xml';
        $containerXml = simplexml_load_string(file_get_contents($containerXmlPath));
        $metaInfoXmlPath = $tmpPath . $containerXml->rootfiles->rootfile->attributes()->{'full-path'}[0];
        $metaInfoXml = simplexml_load_string(file_get_contents($metaInfoXmlPath));
        $metaInfoXml->registerXPathNamespace('dc', 'http://purl.org/dc/elements/1.1/');
        $this->deleteTempDir($tmpPath);

        $bookMetadata = (new BookMetadata())
            ->setTitle((string)$metaInfoXml->xpath('//dc:title')[0])
            ->setAuthorName((string)$metaInfoXml->xpath('//dc:creator')[0])
            ->setLang((string)$metaInfoXml->xpath('//dc:language')[0]);

        return $bookMetadata;
    }

    private function deleteTempDir(string $dirPath): void
    {
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($dirPath, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        foreach ($files as $fileInfo) {
            $functionToExecute = ($fileInfo->isDir() ? 'rmdir' : 'unlink');
            $functionToExecute($fileInfo->getRealPath());
        }

        rmdir($dirPath);
    }
}