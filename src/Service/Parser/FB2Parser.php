<?php

namespace App\Service\Parser;

use App\DTO\BookMetadata;

class FB2Parser extends AbstractBookParser
{
    /**
     * @return BookMetadata
     */
    public function parse(): BookMetadata
    {
        $xml = simplexml_load_string(file_get_contents($this->file->getRealPath()));
        $titleInfo = $xml->description->{'title-info'};
        $authorNode = $titleInfo->author;

        $bookMetadata = (new BookMetadata())
            ->setTitle($titleInfo->{'book-title'})
            ->setAuthorName($authorNode->{'first-name'} . ' ' . $authorNode->{'last-name'})
            ->setLang($titleInfo->lang);

        return $bookMetadata;
    }
}