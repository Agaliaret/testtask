<?php

namespace App\Service\Parser;

use App\DTO\BookMetadata;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractBookParser
{
    /** @var UploadedFile */
    protected UploadedFile $file;

    /**
     * AbstractBookParser constructor.
     * @param $file
     */
    public function __construct(UploadedFile $file)
    {
        $this->file = $file;
    }

    abstract public function parse(): BookMetadata;
}