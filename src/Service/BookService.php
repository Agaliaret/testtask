<?php

namespace App\Service;

use App\DTO\BookMetadata;
use App\Entity\Author;
use App\Entity\Book;
use App\Service\Factory\BookParserFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BookService
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;

    /**
     * BookProcessor constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function parseBook(UploadedFile $bookFile): void
    {
        $parser = BookParserFactory::createByBookFile($bookFile);
        $bookDto = $parser->parse();
        $this->saveDataToDB($bookDto);
    }

    private function saveDataToDB(BookMetadata $bookDto): void
    {
        $authorRepo = $this->em->getRepository(Author::class);
        $author = $authorRepo->findOneBy(['name' => $bookDto->getAuthorName()]);
        if ($author === null) {
            $author = (new Author())->setName($bookDto->getAuthorName());
            $this->em->persist($author);
        }

        $bookRepo = $this->em->getRepository(Book::class);
        $book = $bookRepo->findOneBy(['title' => $bookDto->getTitle()]);
        if ($book === null) {
            $book = (new Book())
                ->setTitle($bookDto->getTitle())
                ->setLang($bookDto->getLang())
                ->setAuthor($author);
            $this->em->persist($book);
        }

        $this->em->flush();
    }
}