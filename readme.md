Разворачивание проекта:
1. Запустить из папки docker команду ```docker-compose up -d --build```
2. Подключиться к bash внутри контейнера командой ```docker-compose exec web-app bash``` и выполнить следующие команды:
    - ```composer install```
    - ```php bin/console doctrine:migrations:migrate```
3. Существует три раута: 
    - http://localhost:8000/book/parse - раут для загрузки в систему данных по новым книгам (примеры файлов можно найти в папке file_examples)
    - http://localhost:8000/book/search - раут для поиска книги по названию
    - http://localhost:8000/book/statistics - раут для просмотра статистики по системе